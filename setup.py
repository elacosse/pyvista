"""
To install pyvista correctly, the via shared object file must be copied
to the same location where the modules of pyvista are installed.
This is accomplished via the data_files entry in the setup below.

To compile .so file, run
$ python setup.py build_ext --inplace
"""

from distutils.core import setup, Extension
import numpy
from distutils.sysconfig import get_python_lib
#include "viaio/Vlib.h"
#include "viaio/mu.h"
#include "viaio/os.h"
#include "viaio/VImage.h"# define the extension module

extensions = [
    Extension("pyvista", ["pyvista/pyvista.c"],
        include_dirs = ["./include", numpy.get_include()], # C_INCLUDE_PATH (.h files) -I
        library_dirs = ["./lib"], # LD_LIBRARY_PATH (.so file) -L (Maybe change to LIPSIA directory?)
	libraries = ["viaio3.0", "gsl", "gslcblas", "z"]), # -lviaio -lgsl -gslcblas
]

setup(
    name = "pyvista",
    author = 'Eric Lacosse' ,
    author_email = 'eric.lacosse@tuebingen.mpg.de' ,
    description = 'a package for importing vista file format' ,
    version = '0.0.1' ,
    packages = ['pyvista'] ,
    py_modules = ['pyvista'] ,
    license = 'GNU GENERAL PUBLIC LICENSE version 2 or higher' ,
    platforms = ['linux2'] ,
    data_files = [(get_python_lib()+'/pyvista/', ['pyvista.so']),
                 ],
    #long_description=open('README.txt').read()
    ext_modules = extensions,
)
