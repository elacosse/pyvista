# pyvista_test.py
import sys, os.path
sys.path.append("../")
import pyvista
import nibabel as nb
import numpy as np



def getAffine(image, voxel_size=None, sform=None, qform=None):
    if sform != None:
        return None
    if qform != None:
        return None
    return shape_zoom_affine(image.shape, voxel_size)

[return_img0, dim, pixdim, sform_code, sform, qform_code, qform, voxel_res,
    quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp,
    extent] = pyvista.loadVImage("../anatomical.v")
#[voxel_res, quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp, extent] = pyvista.loadVImage("./ref.v")

print "################ VISTA 1 ####################"
print
print
print "image " + str(return_img0.shape)
print "dim " + str(dim)
print "pixdim " + str(pixdim)
print "sform_code " + str(sform_code)
print "sform " + str(sform)
print "qform_code " + str(qform_code)
print "qform " + str(qform)


print voxel_res
print quatern
print qoffset
print column_vec
print row_vec
print slice_vec
print index_origin
print ca
print cp
print extent


print "################ VISTA 2 ####################"
print
print

[return_img1, dim, pixdim, sform_code, sform, qform_code, qform, voxel_res,
    quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp, extent] = pyvista.loadVImage("../anatomical.v")


#[voxel_res, quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp, extent] = pyvista.loadVImage("./ref.v")

print "image " + str(return_img1.shape)
print "dim " + str(dim)
print "pixdim " + str(pixdim)
print "sform_code " + str(sform_code)
print "sform " + str(sform)
print "qform_code " + str(qform_code)
print "qform " + str(qform)


print voxel_res
print quatern
print qoffset
print column_vec
print row_vec
print slice_vec
print index_origin
print ca
print cp
print extent


print "################ VISTA 2 Functional ####################"
print
print

#[return_img2, dim, pixdim, sform_code, sform, qform_code, qform] = pyvista.loadVFuncImage("../run1.v")
[return_img2, dim, pixdim, sform_code, sform, qform_code, qform, voxel_res,
    quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp,
    extent] = pyvista.loadVImage("../run1.v")
#[return_img2, dim, pixdim, sform_code, sform, qform_code, qform, voxel_res,
#    quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp, extent] = pyvista.loadVImage("../run1.v")

#[voxel_res, quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp, extent] = pyvista.loadVImage("./ref.v")

print "image " + str(return_img2.shape)
print "dim " + str(dim)
print "pixdim " + str(pixdim)
print "sform_code " + str(sform_code)
print "sform " + str(sform)
print "qform_code " + str(qform_code)
print "qform " + str(qform)

img = return_img2[:,:,:,5]
def plot_slice(data, aff, z_idx=5):

    # Find the center of the brain matrix
    ctr = np.dot(np.linalg.inv(aff), [0, 0, 0, 1])[:3]

    # Plot the data
    plt.imshow(data[:, :, ctr[2] + z_idx],
               cmap="gray")
    plt.gca().set_axis_off()
#orig_img = nb.load("./.nii")
#print orig_img.header
#print orig_img.affine
