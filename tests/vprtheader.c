/*
** Print geometry info conained in vista image header
*/
#include <viaio/Vlib.h>
#include <viaio/VImage.h>
#include <viaio/mu.h>
#include <viaio/option.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


void VPrtHeaderInfo(VAttrList geolist)
{
  VBundle bundle=NULL;
  VImage rotimage=NULL;
  int i,j;
  float dim[6];
  float pixdim[6];
  float qform[6];
  int qform_code=0;
  int sform_code=0;

  fprintf(stderr,"\n geometry info:\n\n");

  if (VGetAttr (geolist, "dim", NULL,VBundleRepn, (VPointer) & bundle) != VAttrFound)
    VError(" dim not found");
  float *E = bundle->data;
  for (i=0; i<8; i++) dim[i] = E[i];
  fprintf(stderr," dim: ");
  for (i=0; i<8; i++) fprintf(stderr," %.4f",dim[i]);
  fprintf(stderr,"\n\n");

  if (VGetAttr (geolist, "pixdim", NULL,VBundleRepn, (VPointer) & bundle) != VAttrFound)
    VError(" pixdim not found");
  float *D = bundle->data;
  for (i=0; i<8; i++) pixdim[i] = D[i];


  fprintf(stderr," pixdim: ");
  for (i=0; i<8; i++) fprintf(stderr," %.4f",pixdim[i]);
  fprintf(stderr,"\n\n");

  VGetAttr (geolist, "sform_code", NULL,VShortRepn, (VPointer) & sform_code);
  fprintf(stderr," sform_code: %d\n",sform_code);
  if (sform_code > 0) {
    if (VGetAttr (geolist, "sform", NULL,VImageRepn, (VPointer) & rotimage) != VAttrFound)
      VError(" sform not found");
    fprintf(stderr," sform: \n");
    for (i=0; i<4; i++) {
      for (j=0; j<4; j++) {
	double u = VGetPixel(rotimage,0,i,j);
	fprintf(stderr," %10.3f",u);
      }
      fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n");
  }

  VGetAttr (geolist, "qform_code", NULL,VShortRepn, (VPointer)&qform_code);
  fprintf(stderr," qform_code: %d\n",qform_code);
  if (qform_code > 0) {
    if (VGetAttr (geolist, "qform", NULL,VBundleRepn, (VPointer) & bundle) != VAttrFound)
      VError(" qform not found");
    float *Q = bundle->data;
    for (i=0; i<6; i++) qform[i] = Q[i];

    fprintf(stderr," qform: ");
    for (i=0; i<6; i++) fprintf(stderr," %.4f",qform[i]);
    fprintf(stderr,"\n");
  }
  fprintf(stderr,"\n");
}




int main(int argc,char *argv[])
{
  FILE *in_file;
  VAttrList list=NULL,geolist=NULL;
  VAttrListPosn posn;


  /* parse command line */
  VParseFilterCmd (0,NULL, argc, argv, & in_file,NULL);

  /* read input data */
  list = VReadFile (in_file,NULL);
  if (! list) VError(" can't read src file");
  fclose(in_file);

  for (VFirstAttr (list, & posn); VAttrExists (& posn); VNextAttr (& posn)) {
    if (strncmp(VGetAttrName(&posn), "imageinfo", strlen("imageinfo")) == 0 ) {
      VGetAttrValue (& posn, NULL, VAttrListRepn, & geolist);
      VPrtHeaderInfo(geolist);
    }
  }

  exit(0);
}
