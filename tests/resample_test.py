import sys, os
sys.path.append('/home/eric/repos/pyvista')

import pyvista
import nibabel as nb
import nipy


file_dir = '/mnt/cc902fe0-3b41-4b2a-b5c3-e52420f39d51/mri_data/templates_tests/'

img_anat_nii = nb.load(file_dir + 'mni_t1.nii')
[img_data_anat_v, dim, pixdim, sform_code, sform, qform_code, qform,
voxel_res, quatern, qoffset, column_vec, row_vec, slice_vec,
index_origin, ca, cp, extent] = pyvista.loadVImage(file_dir + 'mni_t1.v')
img_overlay_1 = nb.load(file_dir + 'zmap.nii')
img_overlay_2 = nb.load(file_dir + 'ts.nii')
img_overlay_3 = nb.load(file_dir + 'mean.nii.gz')


# Try nipy resampling routines.
from nipy.algorithms.registration import resample
resampled_img = resample(img_overlay_1, reference=img_anat_nii)
print "reference image affine and shape: " + str(img_anat_nii.shape)
print img_anat_nii.affine
print "orig. image affine and shape: " + str(img_overlay_1.shape)
print img_overlay_1.affine
print "resampled image affine and shape : " + str(resampled_img.shape)
print resampled_img.affine
#zoomFactors = [bi/float(ai) for ai, bi in zip(a, b)]
#downed = nd.interpolation.zoom(flash, zoom=zoomFactors)

# create new nibabel nifti image object from vista image data

print dir(resampled_img)
print resampled_img.get_data()[30:60, 30:60, :]

#from nilearn import plotting as nlplot
#nlplot.plot_anat(img_anat_nii)
#nlplot.plot_stat_map(resampled_img, img_anat_nii)
