# pyvista

pyvista is a python utility written as a C extension module for the importing
of [vista](!http://www.cs.ubc.ca/nest/lci/vista/vista.html) images as a numpy data array.
At the moment, it only supports importing vista images.

# Building and Installation
The following libraries are needed for building the vista library.
  * ``viaio``
  * ``gsl``
  * ``gslcblas``

Installing ``gsl-bin`` and ``libgsl-dev`` can be easily done on linux systems.
To install the viaio library in the local directory, change directory to ``/lib_viaio`` directory, run ``make``.
This should creates a shared object, ``libviaio.so`` in ``lib/``

Note: This is independent from the libviaio built in the LIPSIA project.

Next, for building the python C module run

``python setup.py build_ext --inplace``

This should now create ``pyvista.so`` in pyvista.

To install for import within python, it needs to be install into site-packages.

This can be done by
``python setup.py install``

Lastly, the library path needs to be added to the bashrc (~/.bashrc), by adding the following line:
export LD_LIBRARY_PATH=/INSERTPATHTOPYVISTA/pyvista/lib/:$LD_LIBRARY_PATH

# Functionality
``loadVImage("path to image")``

``loadVFuncImage("path to image")``

``load_vimage`` returns a list of image attributes,
`` [return_img, dim, pixdim, sform_code, sform, qform_code, qform, voxel_res,
    quatern, qoffset, column_vec, row_vec, slice_vec, index_origin, ca, cp, extent] ``
