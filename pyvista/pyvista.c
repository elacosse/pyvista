#include <stdio.h>
#include <stdlib.h>
#include <Python.h>
#include <numpy/arrayobject.h>

#include <locale.h>

/* From the Vista library: */
#include "viaio/Vlib.h"
#include "viaio/mu.h"
#include "viaio/os.h"
#include "viaio/VImage.h"

/* From the standard C libaray: */
#include <math.h>

/* From nifti and vnifti */
#include "nifti/nifti1_io.h"

/* functional read functions */
#include "readfuncdata.h"

#define MIN_HEADER_SIZE 348
#define NII_HEADER_SIZE 352
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

static char module_docstring[] =
    "This module provides an interface for importing vista files to use in numpy";
static char loadVImage_docstring[] =
    "Read in a 3D vista image and return a python object";
static char loadVFuncImage_docstring[] =
    "Read in a 4D vista image and return a python object";


static PyObject *loadVImage( PyObject *self, PyObject *args )
{


  /* Type conversion from vista file to python numpy array */
  /* Parse string input handed over from python */
  char *filename;
  if (!PyArg_ParseTuple(args, "s", &filename))
    return NULL;

  /*int itype = getformat(filename);
  if (!(itype == 0))
    VError("Vista Error: input file is not a Vista type! Inspect image.");
  */

  int nimages = 0;

  /*VImage image = NULL;*/
  VAttrListPosn posn;
  FILE *fp = VOpenInputFile(filename, TRUE);
  VAttrList list = VReadFile(fp, NULL);
  if (! list) {
    VError(" Cannot read input file!");
    exit(1);
  }
  fclose(fp);

  /* Check image dimensions */
  int vimg_ver = 0;
  VAttrList geolist = NULL;
  for (VFirstAttr (list, & posn); VAttrExists (& posn); VNextAttr (& posn)) {
    if (strncmp(VGetAttrName(&posn), "geoinfo", strlen("geoinfo")) == 0 )
      VGetAttrValue (& posn, NULL, VAttrListRepn, & geolist);
  }
  if (geolist != NULL) { vimg_ver = 2; }
  else { vimg_ver = 1; }
  float t_dim = 0;
  if (vimg_ver == 2) {
    /* Read in dim info */
    VBundle bundle=NULL;
    if (VGetAttr (geolist, "dim", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
      VError(" dim not found");
    float *E = bundle->data;
    t_dim = E[4];
  }

  if (t_dim < 2.0) {

    /* Step through attribute list and read imageinfo header as geolist */
    /* VAttrList geolist VGetGeoInfo(VAttrList list) */
    /*VDestroyImage(image);*/
    VImage image = NULL;
    int vimg_ver = 0;
    VAttrList geolist = NULL;
    for (VFirstAttr (list, & posn); VAttrExists (& posn); VNextAttr (& posn)) {
      if (strncmp(VGetAttrName(&posn), "geoinfo", strlen("geoinfo")) == 0 )
        VGetAttrValue (& posn, NULL, VAttrListRepn, & geolist);
    }
    if (geolist != NULL) { vimg_ver = 2; }
    else { vimg_ver = 1; }

    /* printf("VERSION %d VIMAGE \n", vimg_ver); */

    VBundle bundle=NULL;
    VImage rotimage=NULL;
    int i,j;
    int qf_code=0;
    int sf_code=0;
    int k;
    double u;
    PyObject *dict = NULL;

    /* dim, pixdim, s_form, s_form_code, q_form, q_form_code */
    PyObject* dim; /* = PyList_New(8); */
    PyObject* pixdim;/* = PyList_New(8); */
    PyIntObject* sform_code = 0;
    PyObject* sform = 0;
    PyIntObject* qform_code = 0;
    PyObject* qform;/* = PyList_New(6); */
    if (vimg_ver == 2) {
      /* Read in dim info */
      dim = PyList_New(8);
      if (VGetAttr (geolist, "dim", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
        VError(" dim not found");
      float *E = bundle->data;
      for (i=0; i<8; i++) {
        dict = Py_BuildValue("f", E[i]);
        PyList_SetItem(dim, i, dict);
      }

      /* read in pixdim */
      pixdim = PyList_New(8);
      if (VGetAttr (geolist, "pixdim", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
        VError(" pixdim not found");
      float *D = bundle->data;
      for (i=0; i<8; i++) {
        dict = Py_BuildValue("f", D[i]);
        PyList_SetItem(pixdim, i, dict);
      }

      /* read in s_form code */
      /* PyIntObject* sform_code = 0; */
      /* PyObject* sform = 0;*/
      double (*sform_array)[4] = malloc(4 * sizeof *sform_array);
      for (i=0; i < 4; i++) {
        for (j=0; j < 4; j++) {
          sform_array[i][j] = 0.0;
        }
      }
      sform_array[3][3] = 1;
      VGetAttr (geolist, "sform_code", NULL, VShortRepn, (VPointer) & sf_code);
      sform_code = sf_code;
      if (sf_code > 0) {
        if (VGetAttr (geolist, "sform", NULL, VImageRepn, (VPointer) & rotimage) != VAttrFound)
          VError(" sform not found");
          /* int i, j, k; */
          for (i=0; i < 4; i++) {
            for (j=0; j < 4; j++) {
              u = VGetPixel(rotimage, 0, i, j);
              sform_array[i][j] = u;
            }
          }
      }
      npy_intp dimension[] = {4, 4};
      sform = PyArray_SimpleNewFromData(2, dimension, NPY_DOUBLE, sform_array);

      /* read in q_form code */
      qform = PyList_New(6);
      VGetAttr (geolist, "qform_code", NULL, VShortRepn, (VPointer) &qf_code);
      qform_code = qf_code;
      if (qform_code > 0) {
        if (VGetAttr (geolist, "qform", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
          VError(" qform not found");
        float *Q = bundle->data;
        for (i=0; i<6; i++) {
          dict = Py_BuildValue("f", Q[i]);
          PyList_SetItem(qform, i, dict);
        }
      }
    } else {

      /* set version 2 header attr to default values (list with 0 integer) or int */
      dim = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(dim, i, dict);
      pixdim = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(pixdim, i, dict);
      sform = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(sform, i, dict);
      qform = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(qform, i, dict);
      /* PyIntObject* sform_code = 0; */
      /* PyIntObject* qform_code = 0; */
    }

    /* Step through attribute list and read image header */
    for (VFirstAttr(list, & posn); VAttrExists(& posn); VNextAttr(& posn)) {
      if (VGetAttrRepn(& posn) != VImageRepn)
        continue;
      VGetAttrValue(& posn, NULL, VImageRepn, &image);
      nimages++;
      break;
    }

    int nslices = VImageNBands(image);
    int nrows = VImageNRows(image);
    int ncolumns = VImageNColumns(image);

    /*printf("i : %d, j : %d, k : %d \n", nslices, nrows, ncolumns);*/

    /* get data type */
    VRepnKind repn = VPixelRepn(image);
    int datatype = 0;

    switch (repn) {

    case VBitRepn:
      datatype = DT_BINARY;
      break;
    case VUByteRepn:
      datatype = DT_UNSIGNED_CHAR;
      break;
    case VShortRepn:
      datatype = DT_SIGNED_SHORT;
      break;
    case VLongRepn:
      datatype = DT_SIGNED_INT;
      break;
    case VFloatRepn:
      datatype = DT_FLOAT;
      break;
    case VUShortRepn:
      datatype = DT_UINT16; /* no unsigned char name code */
      break;
    case VIntegerRepn:
      datatype = DT_SIGNED_INT;
      break;
    case VUIntegerRepn:
      datatype = DT_UINT32;
      break;
    case VULongRepn:
      datatype = DT_UINT64;
      break;
    default:
      VError(" unknown datatype");
    }

    /* get voxel resolution */
    /* PyObject *dict = NULL; */
    char *buf = (char *) VCalloc(512,sizeof(char));
    float vox_x, vox_y , vox_z;
    setlocale(LC_NUMERIC, "C");
    if (VGetAttr (VImageAttrList (image), "voxel", NULL, VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf,"%f %f %f", &vox_x, &vox_y, &vox_z);
    }
    /* printf("voxel x y z: %f, %f, %f \n", vox_x, vox_y, vox_z); */
    PyObject* voxel_res = PyList_New(4); /* bitpix, voxel resolutions x, y, z */
    dict = Py_BuildValue("f", (double) VPixelPrecision(image));
    PyList_SetItem(voxel_res, 0, dict);
    dict = Py_BuildValue("f", vox_x);
    PyList_SetItem(voxel_res, 1, dict);
    dict = Py_BuildValue("f", vox_y);
    PyList_SetItem(voxel_res, 2, dict);
    dict = Py_BuildValue("f", vox_z);
    PyList_SetItem(voxel_res, 3, dict);

    VFloat tmp=0;
    VFloat qb=1.0, qc=0.0, qd=0.0;
    if (VGetAttr(VImageAttrList(image), "quatern_b", NULL, VFloatRepn,(VPointer)&tmp) == VAttrFound) qb = tmp;
    if (VGetAttr(VImageAttrList(image), "quatern_c", NULL, VFloatRepn,(VPointer)&tmp) == VAttrFound) qc = tmp;
    if (VGetAttr(VImageAttrList(image), "quatern_d", NULL, VFloatRepn,(VPointer)&tmp) == VAttrFound) qd = tmp;
    PyObject* quatern = PyList_New(3); /* quatern [b c d] */
    dict = Py_BuildValue("f", qb);
    PyList_SetItem(quatern, 0, dict);
    dict = Py_BuildValue("f", qc);
    PyList_SetItem(quatern, 1, dict);
    dict = Py_BuildValue("f", qd);
    PyList_SetItem(quatern, 2, dict);

    VFloat qoffx=0, qoffy=0, qoffz=0;
    if (VGetAttr(VImageAttrList(image), "qoffset_x", NULL, VFloatRepn,(VPointer)&tmp) == VAttrFound) qoffx = tmp;
    if (VGetAttr(VImageAttrList(image), "qoffset_y", NULL, VFloatRepn,(VPointer)&tmp) == VAttrFound) qoffy = tmp;
    if (VGetAttr(VImageAttrList(image), "qoffset_z", NULL, VFloatRepn,(VPointer)&tmp) == VAttrFound) qoffz = tmp;
    PyObject* qoffset = PyList_New(3); /* qoffset [x y z] */
    dict = Py_BuildValue("f", qoffx);
    PyList_SetItem(qoffset, 0, dict);
    dict = Py_BuildValue("f", qoffy);
    PyList_SetItem(qoffset, 1, dict);
    dict = Py_BuildValue("f", qoffz);
    PyList_SetItem(qoffset, 2, dict);

    VFloat col_vec_x=0, col_vec_y=0, col_vec_z=0;
    if (VGetAttr (VImageAttrList (image), "columnVec", NULL,VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%f %f %f", &col_vec_x, &col_vec_y, &col_vec_z);
    }
    PyObject* column_vec = PyList_New(3);
    dict = Py_BuildValue("f", col_vec_x);
    PyList_SetItem(column_vec, 0, dict);
    dict = Py_BuildValue("f", col_vec_y);
    PyList_SetItem(column_vec, 1, dict);
    dict = Py_BuildValue("f", col_vec_z);
    PyList_SetItem(column_vec, 2, dict);

    VFloat row_vec_x=0, row_vec_y=0, row_vec_z=0;
    if (VGetAttr (VImageAttrList (image), "rowVec", NULL,VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%f %f %f", &row_vec_x, &row_vec_y, &row_vec_z);
    }
    PyObject* row_vec = PyList_New(3);
    dict = Py_BuildValue("f", row_vec_x);
    PyList_SetItem(row_vec, 0, dict);
    dict = Py_BuildValue("f", row_vec_y);
    PyList_SetItem(row_vec, 1, dict);
    dict = Py_BuildValue("f", row_vec_z);
    PyList_SetItem(row_vec, 2, dict);

    VFloat slice_vec_x=0, slice_vec_y=0, slice_vec_z=0;
    if (VGetAttr (VImageAttrList (image), "sliceVec", NULL,VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%f %f %f", &slice_vec_x, &slice_vec_y, &slice_vec_z);
    }
    PyObject* slice_vec = PyList_New(3);
    dict = Py_BuildValue("f", slice_vec_x);
    PyList_SetItem(slice_vec, 0, dict);
    dict = Py_BuildValue("f", slice_vec_y);
    PyList_SetItem(slice_vec, 1, dict);
    dict = Py_BuildValue("f", slice_vec_z);
    PyList_SetItem(slice_vec, 2, dict);

    VFloat index_origin_x=0, index_origin_y=0, index_origin_z=0;
    if (VGetAttr (VImageAttrList (image), "indexOrigin", NULL,VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%f %f %f", &index_origin_x, &index_origin_y, &index_origin_z);
    }
    PyObject* index_origin = PyList_New(3);
    dict = Py_BuildValue("f", index_origin_x);
    PyList_SetItem(index_origin, 0, dict);
    dict = Py_BuildValue("f", index_origin_y);
    PyList_SetItem(index_origin, 1, dict);
    dict = Py_BuildValue("f", index_origin_z);
    PyList_SetItem(index_origin, 2, dict);

    int ca_x=0, ca_y=0, ca_z=0;
    if (VGetAttr (VImageAttrList (image), "ca", NULL, VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%d %d %d", &ca_x, &ca_y, &ca_z);
    }
    PyObject* ca = PyList_New(3);
    dict = Py_BuildValue("i", ca_x);
    PyList_SetItem(ca, 0, dict);
    dict = Py_BuildValue("i", ca_y);
    PyList_SetItem(ca, 1, dict);
    dict = Py_BuildValue("i", ca_z);
    PyList_SetItem(ca, 2, dict);

    int cp_x=0, cp_y=0, cp_z=0;
    if (VGetAttr (VImageAttrList (image), "cp", NULL, VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%d %d %d", &cp_x, &cp_y, &cp_z);
    }
    PyObject* cp = PyList_New(3);
    dict = Py_BuildValue("i", cp_x);
    PyList_SetItem(cp, 0, dict);
    dict = Py_BuildValue("i", cp_y);
    PyList_SetItem(cp, 1, dict);
    dict = Py_BuildValue("i", cp_z);
    PyList_SetItem(cp, 2, dict);

    int extent_x=0, extent_y=0, extent_z=0;
    if (VGetAttr (VImageAttrList (image), "extent", NULL, VStringRepn, (VPointer) & buf) == VAttrFound) {
      sscanf(buf, "%d %d %d", &extent_x, &extent_y, &extent_z);
    }
    PyObject* extent = PyList_New(3);
    dict = Py_BuildValue("i", extent_x);
    PyList_SetItem(extent, 0, dict);
    dict = Py_BuildValue("i", extent_y);
    PyList_SetItem(extent, 1, dict);
    dict = Py_BuildValue("i", extent_z);
    PyList_SetItem(extent, 2, dict);

    double (*image_array)[nrows][nslices] = malloc(ncolumns * sizeof *image_array);
    /* int i, j, k; */
    /* int k; */
    /* double u; */
    for (i=0; i < ncolumns; i++) {
      for (j=0; j < nrows; j++) {
        for (k=0; k < nslices; k++) {
          u = VGetPixel(image, k, j, i);
          /* image_array[ncolumns-i-1][nrows-j-1][nslices-k-1] = u; */
          image_array[i][j][k] = u;
          /* image_array[i][nrows-j-1][nslices-k-1] = u; flip image */
        }
      }
    }
    VDestroyImage(image);
    int nd = 3;
    npy_intp dims[] = {ncolumns, nrows, nslices};
    PyObject* return_img = PyArray_SimpleNewFromData(nd, dims, NPY_DOUBLE, image_array);

    /* return Py_BuildValue("OOOOOOO", return_img, dim, pixdim, sform_code, sform, qform_code, qform); */

    return Py_BuildValue("OOOiOiOOOOOOOOOOO", return_img, dim, pixdim, sform_code,
                          sform, qform_code, qform, voxel_res, quatern, qoffset,
                          column_vec, row_vec, slice_vec, index_origin, ca, cp, extent);

  }

  else {
    /* Step through attribute list and read imageinfo header as geolist */
    /* VAttrList geolist VGetGeoInfo(VAttrList list) */

    int vimg_ver = 0;
    VAttrList geolist = NULL;
    for (VFirstAttr (list, & posn); VAttrExists (& posn); VNextAttr (& posn)) {
      if (strncmp(VGetAttrName(&posn), "geoinfo", strlen("geoinfo")) == 0 )
        VGetAttrValue (& posn, NULL, VAttrListRepn, & geolist);
    }
    if (geolist != NULL) { vimg_ver = 2; }
    else { vimg_ver = 1; }

    int nt=0, ncolumns=0, nrows=0, nslices=0;
    VImage *image = VImagePointer(list, &nt, &nslices, &nrows, &ncolumns);
    VReadImagePointer(list, image);
    if (nt < 1) {
      VError(" Error: inspect functional image. Tried to read, but failed.");
    }

    /* printf("VERSION %d VIMAGE \n", vimg_ver); */

    VBundle bundle=NULL;
    VImage rotimage=NULL;
    int i,j;
    int qf_code=0;
    int sf_code=0;
    int k;
    double u;
    PyObject *dict = NULL;

    /* dim, pixdim, s_form, s_form_code, q_form, q_form_code */
    PyObject* dim; /* = PyList_New(8); */
    PyObject* pixdim;/* = PyList_New(8); */
    PyIntObject* sform_code = 0;
    PyObject* sform = 0;
    PyIntObject* qform_code = 0;
    PyObject* qform;/* = PyList_New(6); */
    if (vimg_ver == 2) {
      /* Read in dim info */
      dim = PyList_New(8);
      if (VGetAttr (geolist, "dim", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
        VError(" dim not found");
      float *E = bundle->data;
      for (i=0; i<8; i++) {
        dict = Py_BuildValue("f", E[i]);
        PyList_SetItem(dim, i, dict);
      }

      /* read in pixdim */
      pixdim = PyList_New(8);
      if (VGetAttr (geolist, "pixdim", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
        VError(" pixdim not found");
      float *D = bundle->data;
      for (i=0; i<8; i++) {
        dict = Py_BuildValue("f", D[i]);
        PyList_SetItem(pixdim, i, dict);
      }

      /* read in s_form code */
      /* PyIntObject* sform_code = 0; */
      /* PyObject* sform = 0;*/
      double (*sform_array)[4] = malloc(4 * sizeof *sform_array);
      for (i=0; i < 4; i++) {
        for (j=0; j < 4; j++) {
          sform_array[i][j] = 0.0;
        }
      }
      sform_array[3][3] = 1;
      VGetAttr (geolist, "sform_code", NULL, VShortRepn, (VPointer) & sf_code);
      sform_code = sf_code;
      if (sf_code > 0) {
        if (VGetAttr (geolist, "sform", NULL, VImageRepn, (VPointer) & rotimage) != VAttrFound)
          VError(" sform not found");
          /* int i, j, k; */
          for (i=0; i < 4; i++) {
            for (j=0; j < 4; j++) {
              u = VGetPixel(rotimage, 0, i, j);
              sform_array[i][j] = u;
            }
          }
      }
      npy_intp dimension[] = {4, 4};
      sform = PyArray_SimpleNewFromData(2, dimension, NPY_DOUBLE, sform_array);

      /* read in q_form code */
      qform = PyList_New(6);
      VGetAttr (geolist, "qform_code", NULL, VShortRepn, (VPointer) &qf_code);
      qform_code = qf_code;
      if (qform_code > 0) {
        if (VGetAttr (geolist, "qform", NULL, VBundleRepn, (VPointer) & bundle) != VAttrFound)
          VError(" qform not found");
        float *Q = bundle->data;
        for (i=0; i<6; i++) {
          dict = Py_BuildValue("f", Q[i]);
          PyList_SetItem(qform, i, dict);
        }
      }
    } else {
      /* set version 2 header attr to default values (list with 0 integer) or int */
      dim = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(dim, i, dict);
      pixdim = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(pixdim, i, dict);
      sform = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(sform, i, dict);
      qform = PyList_New(1);
      dict = Py_BuildValue("i", 0);
      PyList_SetItem(qform, i, dict);

    }
    /* get voxel resolution */
    /* PyObject *dict = NULL; */
    char *buf = (char *) VCalloc(512,sizeof(char));
    float vox_x=0, vox_y=0 , vox_z=0;
    setlocale(LC_NUMERIC, "C");
    PyObject* voxel_res = PyList_New(4); /* bitpix, voxel resolutions x, y, z */
    dict = Py_BuildValue("f", 0);
    PyList_SetItem(voxel_res, 0, dict);
    dict = Py_BuildValue("f", vox_x);
    PyList_SetItem(voxel_res, 1, dict);
    dict = Py_BuildValue("f", vox_y);
    PyList_SetItem(voxel_res, 2, dict);
    dict = Py_BuildValue("f", vox_z);
    PyList_SetItem(voxel_res, 3, dict);

    VFloat tmp=0;
    VFloat qb=1.0, qc=0.0, qd=0.0;
    PyObject* quatern = PyList_New(3); /* quatern [b c d] */
    dict = Py_BuildValue("f", qb);
    PyList_SetItem(quatern, 0, dict);
    dict = Py_BuildValue("f", qc);
    PyList_SetItem(quatern, 1, dict);
    dict = Py_BuildValue("f", qd);
    PyList_SetItem(quatern, 2, dict);

    VFloat qoffx=0, qoffy=0, qoffz=0;
    PyObject* qoffset = PyList_New(3); /* qoffset [x y z] */
    dict = Py_BuildValue("f", qoffx);
    PyList_SetItem(qoffset, 0, dict);
    dict = Py_BuildValue("f", qoffy);
    PyList_SetItem(qoffset, 1, dict);
    dict = Py_BuildValue("f", qoffz);
    PyList_SetItem(qoffset, 2, dict);

    VFloat col_vec_x=0, col_vec_y=0, col_vec_z=0;
    PyObject* column_vec = PyList_New(3);
    dict = Py_BuildValue("f", col_vec_x);
    PyList_SetItem(column_vec, 0, dict);
    dict = Py_BuildValue("f", col_vec_y);
    PyList_SetItem(column_vec, 1, dict);
    dict = Py_BuildValue("f", col_vec_z);
    PyList_SetItem(column_vec, 2, dict);

    VFloat row_vec_x=0, row_vec_y=0, row_vec_z=0;
    PyObject* row_vec = PyList_New(3);
    dict = Py_BuildValue("f", row_vec_x);
    PyList_SetItem(row_vec, 0, dict);
    dict = Py_BuildValue("f", row_vec_y);
    PyList_SetItem(row_vec, 1, dict);
    dict = Py_BuildValue("f", row_vec_z);
    PyList_SetItem(row_vec, 2, dict);

    VFloat slice_vec_x=0, slice_vec_y=0, slice_vec_z=0;
    PyObject* slice_vec = PyList_New(3);
    dict = Py_BuildValue("f", slice_vec_x);
    PyList_SetItem(slice_vec, 0, dict);
    dict = Py_BuildValue("f", slice_vec_y);
    PyList_SetItem(slice_vec, 1, dict);
    dict = Py_BuildValue("f", slice_vec_z);
    PyList_SetItem(slice_vec, 2, dict);

    VFloat index_origin_x=0, index_origin_y=0, index_origin_z=0;
    PyObject* index_origin = PyList_New(3);
    dict = Py_BuildValue("f", index_origin_x);
    PyList_SetItem(index_origin, 0, dict);
    dict = Py_BuildValue("f", index_origin_y);
    PyList_SetItem(index_origin, 1, dict);
    dict = Py_BuildValue("f", index_origin_z);
    PyList_SetItem(index_origin, 2, dict);

    int ca_x=0, ca_y=0, ca_z=0;
    PyObject* ca = PyList_New(3);
    dict = Py_BuildValue("i", ca_x);
    PyList_SetItem(ca, 0, dict);
    dict = Py_BuildValue("i", ca_y);
    PyList_SetItem(ca, 1, dict);
    dict = Py_BuildValue("i", ca_z);
    PyList_SetItem(ca, 2, dict);

    int cp_x=0, cp_y=0, cp_z=0;
    PyObject* cp = PyList_New(3);
    dict = Py_BuildValue("i", cp_x);
    PyList_SetItem(cp, 0, dict);
    dict = Py_BuildValue("i", cp_y);
    PyList_SetItem(cp, 1, dict);
    dict = Py_BuildValue("i", cp_z);
    PyList_SetItem(cp, 2, dict);

    int extent_x=0, extent_y=0, extent_z=0;
    PyObject* extent = PyList_New(3);
    dict = Py_BuildValue("i", extent_x);
    PyList_SetItem(extent, 0, dict);
    dict = Py_BuildValue("i", extent_y);
    PyList_SetItem(extent, 1, dict);
    dict = Py_BuildValue("i", extent_z);
    PyList_SetItem(extent, 2, dict);
    /* PyIntObject* sform_code = 0; */
    /* PyIntObject* qform_code = 0; */

    /* printf("i : %d, j : %d, k : %d, m : %d \n", nslices, nrows, ncolumns, nt); */

    /* get data type */
    VRepnKind repn = VPixelRepn(image[0]);
    int datatype = 0;
    switch (repn) {

    case VBitRepn:
      datatype = DT_BINARY;
      break;
    case VUByteRepn:
      datatype = DT_UNSIGNED_CHAR;
      break;
    case VShortRepn:
      datatype = DT_SIGNED_SHORT;
      break;
    case VLongRepn:
      datatype = DT_SIGNED_INT;
      break;
    case VFloatRepn:
      datatype = DT_FLOAT;
      break;
    case VUShortRepn:
      datatype = DT_UINT16; /* no unsigned char name code */
      break;
    case VIntegerRepn:
      datatype = DT_SIGNED_INT;
      break;
    case VUIntegerRepn:
      datatype = DT_UINT32;
      break;
    case VULongRepn:
      datatype = DT_UINT64;
      break;
    default:
      VError(" unknown datatype");
    }

    /* slice, row, cloumn, timestep */
    float (*image_array)[nrows][nslices][nt] = malloc(ncolumns * sizeof (*image_array));
    int m = 0;
    for (i=0; i < ncolumns; i++) {
      for (j=0; j < nrows; j++) {
        for (k=0; k < nslices; k++) {
          for (m=0; m < nt; m++) {
            /* image_array[i][j][k][m] = (float)VPixel(image[k], m, j, i, VShort);; */
            image_array[i][j][k][m] = (float)VGetPixel(image[k], m, j, i);
          }
        }
      }
    }
    int nd = 4;
    npy_intp dims[] = {ncolumns, nrows, nslices, nt};
    for (k=0; k < nslices; k++)
      VDestroyImage(image[k]);
    PyObject* return_img = PyArray_SimpleNewFromData(nd, dims, NPY_FLOAT, image_array);

    /* return Py_BuildValue("OOOOOOO", return_img, dim, pixdim, sform_code, sform, qform_code, qform); */

    /*return Py_BuildValue("OOOiOiO", return_img, dim, pixdim, sform_code,
                          sform, qform_code, qform);*/
    return Py_BuildValue("OOOiOiOOOOOOOOOOO", return_img, dim, pixdim, sform_code,
                          sform, qform_code, qform, voxel_res, quatern, qoffset,
                          column_vec, row_vec, slice_vec, index_origin, ca, cp, extent);

  }

  /* return Py_BuildValue("OOOOOOOOOOO", return_img, voxel_res, quatern, qoffset,
     column_vec, row_vec, slice_vec, index_origin, ca, cp, extent); */
}




/*  Define functions in module */
static PyMethodDef pyvista_methods[] =
{
  {"loadVImage", (PyCFunction)loadVImage,
  METH_VARARGS, loadVImage_docstring},
  {NULL, NULL, 0, NULL}
};

/* Module initialization */
PyMODINIT_FUNC initpyvista(void)
{
  (void) Py_InitModule("pyvista", pyvista_methods);
  /* Load numpy functionality */
  import_array(); /* from numpy/arrayobject.h */
}
